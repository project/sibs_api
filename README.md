# SIBS API

SIBS API intends to make a API available for other modules to integrate.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sibs_api).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sibs_api).

## Table of contents

- Requirements
- Installation
- Configuration
- Services
- SPG Examples
- MB Examples
- MBWAY Examples
- Maintainers
- Sponsors


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.


## Configuration

Configure the SIBS API at (/admin/config/sibs-api).


## Services

SIBS API integration has the following services available:

 - sibs_api.spg
 - sibs_api.mb
 - sibs_api.mbway


## SPG Examples

//Creates the spg service

$service = \Drupal::service('sibs_api.spg');

$methods = [
    "REFERENCE", 
    "CARD", 
    "MBWAY" 
];
$amount = 5;
$desc = 'Payment description';
$customerInfo = [
  "client_name" => "Client Name",
  "client_email" => "client.name@hostname.pt",
  "shipping" => [
    "street1" => "Rua 123",
    "street2" => "porta 2",
    "city" => "Lisboa",
    "postcode" => "1200-999",
    "country" => "PT"
  ],
  "billing" => [
    "street1" => "Rua 123",
    "street2" => "porta 2",
    "city" => "Lisboa",
    "postcode" => "1200-999",
    "country" => "PT"
  ]
];

// Prepares the checkout

$checkout = $service->prepareCheckout($amount, $methods, $desc, $customerInfo);
$transactionID = $checkout['transactionID'];

// Redirect user to payment form

$service->redirectToForm($transactionID, $checkout);

//Gets the payment status of a transactionID

$response = $service->getpaymentStatus($transactionID);


## MB Examples

//Creates the mb service

$service = \Drupal::service('sibs_api.mb');

$amount = 5;
$desc = 'Payment description';

// Prepares the checkout

$checkout = $service->prepareCheckout($amount, $desc);
$transactionID = $checkout['transactionID'];

// Generate reference

$service->generateReference($transactionID, $checkout);

//Gets the payment status of a transactionID

$response = $service->getpaymentStatus($transactionID);


## MBWAY Examples

//Creates the mb service

$service = \Drupal::service('sibs_api.mbway');

$amount = 5;
$desc = 'Payment description';

// Prepares the checkout

$checkout = $service->prepareCheckout($amount, $desc);
$transactionID = $checkout['transactionID'];

// Generate payment

$reference = $service->generatePayment($transactionID, $checkout, '351#960000000');

//Gets the payment status of a transactionID

$response = $service->getpaymentStatus($transactionID);


## Maintainers

Current maintainers:
- João mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)
- Ricardo Salvado - [rsalvado](https://www.drupal.org/u/rsalvado)


## Sponsors

This project has been sponsored by:
- Javali ADSI