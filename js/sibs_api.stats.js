(function ($, Drupal) {
  Drupal.behaviors.sibs_api_stats = {
    attach: function (context, settings) {

      $(once('sibs_chartsw', '#charts', context)).each(function (index) {
        google.charts.load('current', { packages: ['corechart', "calendar"] });
        google.charts.setOnLoadCallback(drawCharts);
      });

      function drawCharts() {
        Object.values(settings.stats).forEach(val => {

          if (val.type == 'pie') {
            var data = new google.visualization.DataTable();
            Object.entries(val.columns).forEach(col => {
              const [key, value] = col;
              data.addColumn(key, value);
            });
            data.addRows(val.rows);

            var options = {
              title: val.title,
            };

            var chart = new google.visualization.PieChart(document.getElementById(val.id));
          }
          else if (val.type == 'column') {
            var values = [];
            values.push(val.columns);
            Object.entries(val.rows).forEach(col => {
              const [key, value] = col;
              values.push(value);
            });
            var data = google.visualization.arrayToDataTable(values);

            var options = {
              isStacked: val.stacked,
              title: val.title + ' (' + val.total + ')',
              colors: ['#FF9900','darkGreen','#109618','#DC3912','#0444c4'],
            };

            var chart = new google.visualization.ColumnChart(document.getElementById(val.id));
          }
          else if (val.type == 'map') {

            var values = [];
            Object.entries(val.rows).forEach(col => {
              const [key, value] = col;
              values.push([new Date(key), value]);
            });
            console.log(values);
            var data = new google.visualization.DataTable();
            data.addColumn({ type: 'date', id: 'Date' });
            data.addColumn({ type: 'number', id: 'Reservations' });
            data.addRows(values);

            var options = {
              title: val.title,
            };

            var chart = new google.visualization.Calendar(document.getElementById(val.id));
          }

          chart.draw(data, options);
        });

      }

    }
  };
})(jQuery, Drupal);