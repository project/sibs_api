<?php

namespace Drupal\sibs_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Database\Connection;

/**
 * Class for SIBS Webhook.
 */
class SibsWebHook extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, Connection $database) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->database = $database;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('database')
    );
  }

  /**
   * Gets the remote message.
   */
  public function getMessage() {
    $config = $this->configFactory->get('sibs_api.settings');
    $webhookSecret = $config->get('webhook_secret');

    $data = file_get_contents("php://input");
    $headers = getallheaders();

    if ((!empty($headers['X-Initialization-Vector']) || !empty($headers['x-initialization-vector'])) && (!empty($headers['X-Authentication-Tag']) || !empty($headers['x-authentication-tag'])) && !empty($data)) {
      $iv_from_http_header = !empty($headers['X-Initialization-Vector']) ? $headers['X-Initialization-Vector'] : $headers['x-initialization-vector'];
      $auth_tag_from_http_header = !empty($headers['X-Authentication-Tag']) ? $headers['X-Authentication-Tag'] : $headers['x-authentication-tag'];
      $http_body = $data;

      $result = $this->opensslDecrypt($webhookSecret, $iv_from_http_header, $http_body, $auth_tag_from_http_header);
      $this->moduleHandler->alter('sibs_api_webhook', $result);

      try {
        $this->database->insert('sibs_api_audits')->fields([
          'timestamp' => strtotime('now'),
          'transaction_type' => 'webhook',
          'httpstatus' => 200,
          'transaction_id' => !empty($result->transactionID) ? $result->transactionID : NULL,
          'description' => NULL,
          'error_message' => NULL,
          'request' => NULL,
          'response' => json_encode($result, JSON_PRETTY_PRINT),
        ])->execute();
      }
      catch (\Exception $e) {
      }
    }

    $response = [
      "statusCode" => "200",
      "statusMsg" => "Success",
      "notificationID" => (!empty($result->notificationID)) ? $result->notificationID : NULL,
    ];

    return new JsonResponse($response);
  }

  /**
   * Decrypt message.
   */
  public function opensslDecrypt($webhookSecret, $iv_from_http_header, $http_body, $auth_tag_from_http_header) {
    $key = mb_convert_encoding($webhookSecret, "UTF-8", "BASE64");
    $iv = mb_convert_encoding($iv_from_http_header, "UTF-8", "BASE64");
    $cipher_text = mb_convert_encoding($http_body, "UTF-8", "BASE64");
    $auth_tag = mb_convert_encoding($auth_tag_from_http_header, "UTF-8", "BASE64");
    $result = openssl_decrypt($cipher_text, "aes-256-gcm", $key, OPENSSL_RAW_DATA, $iv, $auth_tag);

    if ($result) {
      $re = '/"(paymentStatus|paymentMethod|transactionID|notificationID)":"((\\\\"|[^"])*)"/i';
      preg_match_all($re, $result, $matches);
      $newjson = '{' . implode(',', $matches[0]) . '}';

      return json_decode($newjson);
    }

    return FALSE;
  }

}
