<?php

namespace Drupal\sibs_api\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class for Sibs pages.
 */
class SibsPages extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Form builder will be used via Dependency Injection.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The requestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database, RendererInterface $renderer, FormBuilderInterface $form_builder, RequestStack $request_stack) {
    $this->database = $database;
    $this->renderer = $renderer;
    $this->formBuilder = $form_builder;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('renderer'),
      $container->get('form_builder'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function audits() {
    $search = $this->requestStack->getCurrentRequest()->query->all();
    $query = $this->database->select('sibs_api_audits', 'a')
      ->fields('a')
      ->orderBy('id', 'DESC');
    $query = $query->extend(PagerSelectExtender::class)->limit(50);
    if (!empty($search['transaction_type'])) {
      $query->condition('transaction_type', $search['transaction_type']);
    }
    if (!empty($search['transaction_id'])) {
      $query->condition('transaction_id', '%' . $search['transaction_id'] . '%', 'LIKE');
    }
    if (!empty($search['description'])) {
      $query->condition('description', '%' . $search['description'] . '%', 'LIKE');
    }
    $results = $query->execute()->fetchAll();

    $header = [
      $this->t('Date'),
      $this->t('Transaction Type'),
      $this->t('HTTP Response'),
      $this->t('Transaction ID'),
      $this->t('Description'),
      $this->t('Name'),
      $this->t('Email'),
      $this->t('Value'),
      $this->t('Error Message'),
      $this->t('Request'),
      $this->t('Response'),
    ];
    $rows = [];
    foreach ($results as $value) {
      if (!empty($value->request) && $value->request != 'null') {
        $request = Link::fromTextAndUrl('Request', Url::fromRoute('sibs_api.audits_detail', [
          'type' => 'request',
          'id' => $value->id,
        ]))->toRenderable();
        $request['#attributes'] = [
          'class' => ['use-ajax', 'context-link', 'addmore'],
          'data-dialog-type' => 'dialog',
          'data-dialog-options' => Json::encode(['width' => '80%']),
        ];
      }
      if (!empty($value->response) && $value->response != 'null') {
        $response = Link::fromTextAndUrl('Response', Url::fromRoute('sibs_api.audits_detail', [
          'type' => 'response',
          'id' => $value->id,
        ]))->toRenderable();
        $response['#attributes'] = [
          'class' => ['use-ajax', 'context-link', 'addmore'],
          'data-dialog-type' => 'dialog',
          'data-dialog-options' => Json::encode(['width' => '80%']),
        ];
      }

      $name = '';
      $mail = '';
      $amount = '';
      if (!empty($value->request)) {
        $requestObject = json_decode($value->request);
        if (!empty($requestObject->customer->customerInfo)) {
          $name = $requestObject->customer->customerInfo->customerName;
          $mail = $requestObject->customer->customerInfo->customerEmail;
        }
        if (!empty($requestObject->transaction->amount)) {
          $amount = $requestObject->transaction->amount->value;
        }
      }

      $rows[] = [
        date('Y-m-d H:i:s', $value->timestamp),
        $value->transaction_type,
        $value->httpstatus,
        $value->transaction_id,
        $value->description,
        $name,
        $mail,
        $amount,
        $value->error_message,
        (!empty($request)) ? $this->renderer->render($request) : '-',
        (!empty($response)) ? $this->renderer->render($response) : '-',
      ];
    }

    return [
      'form' => $this->formBuilder->getForm('Drupal\sibs_api\Form\AuditsForm'),
      'table' => [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ],
      'pager' => [
        '#type' => 'pager',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function stats() {
    $params = $this->requestStack->getCurrentRequest()->query->all();
    $year = (!empty($params['year'])) ? $params['year'] : date('Y');
    $month = (isset($params['month'])) ? $params['month'] : date('n');

    $query = $this->database->select('sibs_api_audits', 'a')
      ->fields('a')
      ->orderBy('id', 'ASC');

    $query->where('YEAR(FROM_UNIXTIME(a.timestamp)) = :year', [':year' => $year]);
    $filter = 'months';
    if (!empty($month)) {
      $filter = 'days';
      $query->where('MONTH(FROM_UNIXTIME(a.timestamp)) = :month', [':month' => $month]);
    }
    $results = $query->orderBy('a.timestamp', 'ASC')->execute()->fetchAll();

    $structured_results = [];
    foreach ($results as $value) {
      $op = $value->transaction_type;
      $type = FALSE;
      $amount = 0;
      switch ($value->transaction_type) {
        case 'prepareCheckout':
          if (!empty($value->response)) {
            $response = json_decode($value->response, TRUE);
            $amount = floatval($response['amount']['value']);
          }
          if (!empty($value->request)) {
            $request = json_decode($value->request, TRUE);
            if (!empty($request['transaction']['paymentType'])) {
              $type = $request['transaction']['paymentType'];
            }
          }
          $op = 'Checkout';
          break;

        case 'capturePayment':
          $op = 'Capture';
          break;

        case 'refundPayment':
          $op = 'Refund';
          break;

        case 'webhook':
          if (!empty($value->response)) {
            $response = json_decode($value->response, TRUE);
            if (!empty($response['paymentStatus']) && $response['paymentStatus'] == 'Success') {
              $op = 'Payment Successful';
            }
            else {
              $op = 'Payment Error';
            }
          }
          break;

        case 'getpaymentStatus':
          if (!empty($value->response)) {
            $response = json_decode($value->response, TRUE);
            if (!empty($response['paymentStatus']) && $response['paymentStatus'] == 'Success') {
              $op = 'Payment Successful';
            }
            else {
              $op = 'Payment Error';
            }
          }
          break;
      }
      $date = ($filter == 'months') ? date('n', $value->timestamp) : date('j', $value->timestamp);
      $final_amount = ($amount > 0 && $type) ? ['type' => $type, 'amount' => $amount] : 0;
      $structured_results[$value->transaction_id][$date][$op][] = $final_amount;
    }

    foreach ($structured_results as $id => $dates) {
      foreach ($dates as $date => $types) {
        if (!empty($types['Payment Successful'])) {
          if (!empty($structured_results[$id][$date]['Checkout'])) {
            $amount = $structured_results[$id][$date]['Checkout'][0];
            if (!empty($amount['type']) && $amount['type'] == 'PURS') {
              foreach ($types['Payment Successful'] as $key => $value) {
                $structured_results[$id][$date]['Payment Successful'][$key] = $amount['amount'];
              }
            }
          }
          if (!empty($structured_results[$id][$date]['Payment Error'])) {
            unset($structured_results[$id][$date]['Payment Error']);
          }
        }
        if (!empty($types['Capture'])) {
          if (!empty($structured_results[$id][$date]['Checkout'])) {
            $amount = $structured_results[$id][$date]['Checkout'][0];
            if (!empty($amount['type']) && $amount['type'] == 'AUTH') {
              foreach ($types['Capture'] as $key => $value) {
                $structured_results[$id][$date]['Capture'][$key] = $amount['amount'];
              }
            }
          }
          if (!empty($structured_results[$id][$date]['Payment Error'])) {
            unset($structured_results[$id][$date]['Payment Error']);
          }
        }
        if (!empty($types['Checkout'])) {
          foreach ($structured_results[$id][$date]['Checkout'] as $key => $value) {
            $structured_results[$id][$date]['Checkout'][$key] = 0;
          }
        }
      }
    }

    foreach ($structured_results as $id => $dates) {
      $success = FALSE;
      $error = FALSE;
      foreach ($dates as $date => $types) {
        if (!empty($types['Payment Successful'])) {
          if ($success == FALSE) {
            $success = TRUE;
            if (count($types['Payment Successful']) > 1) {
              $structured_results[$id][$date]['Payment Successful'] = [
                $structured_results[$id][$date]['Payment Successful'][0],
              ];
            }
          }
          else {
            unset($structured_results[$id][$date]['Payment Successful']);
          }
        }
        elseif (!empty($types['Payment Error'])) {
          if ($error == FALSE) {
            $error = TRUE;
            $structured_results[$id][$date]['Payment Error'] = [
              $structured_results[$id][$date]['Payment Error'][0],
            ];
          }
          else {
            unset($structured_results[$id][$date]['Payment Error']);
          }
        }
        if (empty($structured_results[$id][$date])) {
          unset($structured_results[$id][$date]);
        }
      }
    }

    $final = [];
    foreach ($structured_results as $id => $dates) {
      foreach ($dates as $date => $types) {
        foreach ($types as $type => $amounts) {
          $final[$date][$type][] = $amounts[0];
        }
      }
    }

    $rows = [];
    $columns = [
      '',
      "Refund",
      "Capture",
      "Payment Successful",
      "Payment Error",
      "Checkout",
    ];
    $settings['payments'] = [
      'id' => 'payments',
      'title' => 'SIBS API Payments',
      'type' => 'column',
      'columns' => $columns,
      'rows' => [],
      'stacked' => TRUE,
    ];

    $total = 0;
    $array = ($filter == 'months') ? DateHelper::monthNamesAbbr(TRUE) : DateHelper::days(TRUE);
    foreach ($array as $key => $value) {
      $rows[$key] = [];
      $rows[$key][] = !is_object($value) ? $value . '' : $value->render();
      foreach ($columns as $column) {
        if ($column) {
          $rows[$key][] = (!empty($final[$key][$column])) ? count($final[$key][$column]) : 0;
          $total += (!empty($final[$key][$column])) ? array_sum($final[$key][$column]) : 0;
        }
      }
    }
    $settings['payments']['rows'] = $rows;
    $settings['payments']['total'] = $total . ' ' . $this->t('Income');

    $divs = [];
    foreach (array_keys($settings) as $id) {
      $divs[] = '<div id="' . $id . '"></div>';
    }

    $build['form'] = $this->formBuilder->getForm('Drupal\sibs_api\Form\StatsFilter');
    $build['form']['#weight'] = -10;
    $build['charts'] = [
      '#markup' => '<div id="charts">' . implode('', $divs) . '</div>',
      '#attached' => [
        'drupalSettings' => ['stats' => $settings],
        'library' => ['sibs_api/stats'],
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function auditsDetail($type, $id) {
    $query = $this->database->select('sibs_api_audits', 'a')
      ->fields('a', [$type])
      ->condition('id', $id);
    $result = $query->execute()->fetchField();

    return [
      '#markup' => '<pre>' . $result . '</pre>',
    ];
  }

}
