<?php

namespace Drupal\sibs_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Class for Sibs Form.
 */
class SibsForm extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Stores the tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PrivateTempStoreFactory $temp_store_factory) {
    $this->configFactory = $config_factory;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getForm($id) {
    $config = $this->configFactory->get('sibs_api.settings');
    $tempstore = $this->tempStoreFactory->get('sibs_api');

    $formConfig = $tempstore->get($id);
    if (!$formConfig["redirectUrl"]) {
      $formConfig["redirectUrl"] = $config->get('spg_return_url');
    }

    // Convert redirectUrl to absolute URL.
    if (!UrlHelper::isValid($formConfig["redirectUrl"], TRUE)) {
      $formConfig["redirectUrl"] = Url::fromUserInput(
          $formConfig["redirectUrl"],
          ['absolute' => TRUE]
        )->toString();
    }

    $formConfig["customerData"] = NULL;
    $formConfig["language"] = "pt";
    $formContext = $formConfig['formContext'];

    $formStyle = [
      "layout" => (!empty($config->get('spg_formStyle.layout'))) ? $config->get('spg_formStyle.layout') : 'default',
      "theme" => (!empty($config->get('spg_formStyle.theme'))) ? $config->get('spg_formStyle.theme') : 'default',
      "color" => [
        "primary" => $config->get('spg_formStyle.color.primary'),
        "secondary" => $config->get('spg_formStyle.color.secondary'),
        "border" => $config->get('spg_formStyle.color.border'),
        "surface" => $config->get('spg_formStyle.color.surface'),
        "header" => [
          "text" => (!empty($config->get('spg_formStyle.color.header.text'))) ? $config->get('spg_formStyle.color.header.text') : '#5C5C5C',
          "background" => (!empty($config->get('spg_formStyle.color.header.background'))) ? $config->get('spg_formStyle.color.header.background') : '#fff',
        ],
        "body" => [
          "text" => (!empty($config->get('spg_formStyle.color.body.text'))) ? $config->get('spg_formStyle.color.body.text') : '#5C5C5C',
          "background" => (!empty($config->get('spg_formStyle.color.body.background'))) ? $config->get('spg_formStyle.color.body.background') : '#fff',
        ],
      ],
      "font" => $config->get('spg_formStyle.font'),
    ];

    return [
      '#type' => 'inline_template',
      '#template' => '<form class="paymentSPG" spg-context="{{formContext}}" spg-config="{{formConfig}}" spg-style="{{formStyle}}"><form>',
      '#context' => [
        'formContext' => $formContext,
        'formConfig' => json_encode($formConfig),
        'formStyle' => json_encode($formStyle),
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
