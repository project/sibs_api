<?php

namespace Drupal\sibs_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Component\Utility\Random;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\sibs_api\Event\SibsSpgCheckout;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class for Sibs Spg Service.
 *
 * @package Drupal\sibs_api
 */
class SibsSpgService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Stores the tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Event Dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $http_client, PrivateTempStoreFactory $temp_store_factory, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $configFactory->get('sibs_api.settings');
    $this->httpClient = $http_client;
    $this->tempStoreFactory = $temp_store_factory;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Allow modules to modify the json data before triggering checkout.
   *
   * @param array $json_data
   *   An array to be converted as Json to be sent to Sibs.
   *
   * @return array
   *   The modified array to be converted.
   */
  private function modifyJsonData(array $json_data) {
    $event = new SibsSpgCheckout($json_data);
    $this->eventDispatcher->dispatch($event, SibsSpgCheckout::EVENT_NAME);
    return $event->jsonData;
  }

  /**
   * First request to prepare the checkout.
   *
   * @param float $amount
   *   The payment amount.
   * @param array $methods
   *   The payment method.
   * @param string $description
   *   The payment description.
   * @param array $customerInfo
   *   The customer information.
   * @param string $paymentType
   *   The Payment type.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function prepareCheckout(float $amount, array $methods, string $description, array $customerInfo = [], $paymentType = 'PURS') {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments';
    $date = gmdate('Y-m-d\TH:i:s.v\Z');
    $date_end = gmdate("Y-m-d\TH:i:s.v\Z", strtotime('+' . $this->configFactory->get('spg_validity') . ' days'));
    $random = new Random();
    $json_data = [
      "merchant" => [
        "terminalId" => (int) $this->configFactory->get('terminal_id'),
        "channel" => "web",
        "merchantTransactionId" => $random->name(24, TRUE),
      ],
      "transaction" => [
        "transactionTimestamp" => $date,
        "description" => $description,
        "moto" => FALSE,
        "paymentType" => $paymentType,
        "amount" => [
          "value" => $amount,
          "currency" => "EUR",
        ],
        "paymentMethod" => $methods,
        "paymentReference" => [
          "initialDatetime" => $date,
          "finalDatetime" => $date_end,
          "maxAmount" => [
            "value" => $amount,
            "currency" => "EUR",
          ],
          "minAmount" => [
            "value" => $amount,
            "currency" => "EUR",
          ],
          "entity" => $this->configFactory->get('entity'),
        ],
      ],
    ];

    if (!empty($customerInfo)) {
      $json_data['customer']['customerInfo'] = [
        "customerName" => (!empty($customerInfo['client_name'])) ? $customerInfo['client_name'] : '',
        "customerEmail" => (!empty($customerInfo['client_email'])) ? $customerInfo['client_email'] : '',
        "shippingAddress" => [
          "street1" => (!empty($customerInfo['shipping']['street1'])) ? $customerInfo['shipping']['street1'] : '',
          "street2" => (!empty($customerInfo['shipping']['street2'])) ? $customerInfo['shipping']['street2'] : 'Andar 1',
          "city" => (!empty($customerInfo['shipping']['city'])) ? $customerInfo['shipping']['city'] : '',
          "postcode" => (!empty($customerInfo['shipping']['postcode'])) ? $customerInfo['shipping']['postcode'] : '',
          "country" => (!empty($customerInfo['shipping']['country'])) ? $customerInfo['shipping']['country'] : '',
        ],
        "billingAddress" => [
          "street1" => (!empty($customerInfo['billing']['street1'])) ? $customerInfo['billing']['street1'] : '',
          "street2" => (!empty($customerInfo['billing']['street2'])) ? $customerInfo['billing']['street2'] : 'Andar 1',
          "city" => (!empty($customerInfo['billing']['city'])) ? $customerInfo['billing']['city'] : '',
          "postcode" => (!empty($customerInfo['billing']['postcode'])) ? $customerInfo['billing']['postcode'] : '',
          "country" => (!empty($customerInfo['billing']['country'])) ? $customerInfo['billing']['country'] : '',
        ],
      ];
    }

    // Allow other modules to change this array.
    $json_data = $this->modifyJsonData($json_data);

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'body' => json_encode($json_data, JSON_PRETTY_PRINT),
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'prepareCheckout', NULL, $json_data);
  }

  /**
   * Create the payment form and redirect the user.
   *
   * @param string $id
   *   Transaction_id.
   * @param array $context
   *   Payment context.
   * @param mixed $return_url
   *   Return URL.
   */
  public function redirectToForm(string $id, array $context, mixed $return_url = NULL) {
    $tempstore = $this->tempStoreFactory->get('sibs_api');
    $context['redirectUrl'] = $return_url;
    $tempstore->set($id, $context);

    $path = Url::fromRoute('sibs_api.form', ['id' => $id])->toString();
    $response = new RedirectResponse($path);
    $response->send();
    exit;
  }

  /**
   * Check the status of your transaction making a GET request.
   *
   * @param string $id
   *   Payment_id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function getpaymentStatus(string $id) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments/' . $id . '/status';

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'getpaymentStatus', $id, NULL);
  }

  /**
   * Cancel Payment.
   *
   * When canceling a payment and, if sent within the time-frame, the
   *   authorization is not captured yet, instead it causes an authorization
   *   reversal request to be sent to the card issuer to clear the funds held
   *   against the authorization.
   *
   * If requested over a MULTIBANCO Reference generated but not paid, the
   *   reference will be cancelled.
   *
   * @param string $id
   *   Payment_id.
   * @param float $amount
   *   Amount.
   * @param string $description
   *   Description.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function cancelPayment(string $id, float $amount, string $description) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments/' . $id . '/cancellation';
    $date = gmdate('Y-m-d\TH:i:s.v\Z');
    $random = new Random();
    $json_data = [
      "merchant" => [
        "terminalId" => (int) $this->configFactory->get('terminal_id'),
        "channel" => "web",
        "merchantTransactionId" => $random->name(24, TRUE),
      ],
      "transaction" => [
        "transactionTimestamp" => $date,
        "description" => $description,
        "amount" => [
          "value" => $amount,
          "currency" => "EUR",
        ],
        "originalTransaction" => [
          "id" => $id,
        ],
      ],
    ];

    $request = $this->httpClient->request('POST', $uri, [
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json; charset=utf-8',
        'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
        'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
      ],
      'body' => json_encode($json_data, JSON_PRETTY_PRINT),
      'http_errors' => FALSE,
    ]);

    return _sibs_api_handle_response($request, 'cancelPayment', $id, $json_data);
  }

  /**
   * A capture is used to request a debit to previously authorized transaction.
   *
   * A capture request is performed using a previous payment authorization
   *   (AUTH) payment by referring its transactionID and sending a POST
   *   request over HTTPS to the /payments/{transactionID}/capture endpoint.
   *
   * Captures can occur in different ways.
   *
   * Full, capture the full amount authorized and finish the purchase.
   *
   * Partial, split the capture over one or several capture requests, up to the
   *   total amount authorized.
   *
   * @param string $id
   *   Payment_id.
   * @param float $amount
   *   Amount.
   * @param string $description
   *   Description.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function capturePayment(string $id, float $amount, string $description) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments/' . $id . '/capture';

    $date = gmdate('Y-m-d\TH:i:s.v\Z');
    $random = new Random();
    $json_data = [
      "merchant" => [
        "terminalId" => (int) $this->configFactory->get('terminal_id'),
        "channel" => "web",
        "merchantTransactionId" => $random->name(24, TRUE),
      ],
      "transaction" => [
        "transactionTimestamp" => $date,
        "description" => $description,
        "amount" => [
          "value" => $amount,
          "currency" => "EUR",
        ],
        "originalTransaction" => [
          "id" => $id,
        ],
      ],
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'body' => json_encode($json_data, JSON_PRETTY_PRINT),
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'capturePayment', $id, $json_data);
  }

  /**
   * Refund Payment.
   *
   * The purpose of this operation is to refund the amount of a previous
   *   payment, crediting the cardholder account and debiting the Merchant
   *   account.
   *
   * @param string $id
   *   Payment_id.
   * @param float $amount
   *   Amount.
   * @param string $description
   *   Description.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function refundPayment(string $id, float $amount, string $description) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments/' . $id . '/refund';

    $date = gmdate('Y-m-d\TH:i:s.v\Z');
    $random = new Random();
    $json_data = [
      "merchant" => [
        "terminalId" => (int) $this->configFactory->get('terminal_id'),
        "channel" => "web",
        "merchantTransactionId" => $random->name(24, TRUE),
      ],
      "transaction" => [
        "transactionTimestamp" => $date,
        "description" => $description,
        "amount" => [
          "value" => floatval($amount),
          "currency" => "EUR",
        ],
        "originalTransaction" => [
          "id" => $id,
        ],
      ],
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'body' => json_encode($json_data, JSON_PRETTY_PRINT),
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'refundPayment', $id, $json_data);
  }

}
