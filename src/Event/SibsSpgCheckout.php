<?php

namespace Drupal\sibs_api\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired before checkout is performed.
 */
class SibsSpgCheckout extends Event {

  // This makes it easier for subscribers to reliably use our event name.
  const EVENT_NAME = 'sibs_spg.checkout';

  /**
   * The json data.
   *
   * @var array
   */
  public array $jsonData;

  /**
   * Constructs the object.
   *
   * @param array $json_data
   *   The json data.
   */
  public function __construct(array $json_data) {
    $this->jsonData = $json_data;
  }

}
