<?php

namespace Drupal\sibs_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The audits form.
 */
final class AuditsForm extends FormBase {

  /**
   * The requestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, Connection $database) {
    $this->requestStack = $request_stack;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('request_stack'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'audits_filter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = $this->requestStack->getCurrentRequest()->query->all();

    $form['transaction_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Transaction Type'),
      '#options' => [
        '' => $this->t('All'),
        'prepareCheckout' => $this->t('prepareCheckout'),
        'getpaymentStatus' => $this->t('getpaymentStatus'),
        'webhook' => $this->t('webhook'),
        'cancelPayment' => $this->t('cancelPayment'),
        'refundPayment' => $this->t('refundPayment'),
        'capturePayment' => $this->t('capturePayment'),
      ],
      '#default_value' => (!empty($request['transaction_type'])) ? $request['transaction_type'] : '',
    ];

    $form['transaction_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transaction ID'),
      '#default_value' => (!empty($request['transaction_id'])) ? $request['transaction_id'] : '',
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => (!empty($request['description'])) ? $request['description'] : '',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search', [], ['context' => 'action']),
    ];

    $form['export'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export to csv'),
      '#submit' => [
        [$this, 'exportToCsv'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $query = $this->requestStack->getCurrentRequest()->query->all();

    if (isset($query['page'])) {
      unset($query['page']);
    }

    unset($query['transaction_type']);
    if (!empty($values['transaction_type'])) {
      $query['transaction_type'] = $values['transaction_type'];
    }

    unset($query['transaction_id']);
    if (!empty($values['transaction_id'])) {
      $query['transaction_id'] = $values['transaction_id'];
    }

    unset($query['description']);
    if (!empty($values['description'])) {
      $query['description'] = $values['description'];
    }

    $form_state->setRedirect('sibs_api.audits', [], ['query' => $query]);
  }

  /**
   * {@inheritdoc}
   */
  public function exportToCsv(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $query = $this->database->select('sibs_api_audits', 'a')
      ->fields('a')
      ->orderBy('id', 'DESC');
    if (!empty($values['transaction_type'])) {
      $query->condition('transaction_type', $values['transaction_type']);
    }
    if (!empty($values['transaction_id'])) {
      $query->condition('transaction_id', '%' . $values['transaction_id'] . '%', 'LIKE');
    }
    if (!empty($values['description'])) {
      $query->condition('description', '%' . $values['description'] . '%', 'LIKE');
    }
    $results = $query->execute()->fetchAll();

    $header = [
      $this->t('Date'),
      $this->t('Transaction Type'),
      $this->t('HTTP Response'),
      $this->t('Transaction ID'),
      $this->t('Description'),
      $this->t('Error Message'),
      $this->t('Request'),
      $this->t('Response'),
    ];
    $rows = [];
    foreach ($results as $value) {
      $rows[] = [
        date('Y-m-d H:i:s', $value->timestamp),
        $value->transaction_type,
        $value->httpstatus,
        $value->transaction_id,
        $value->description,
        $value->error_message,
        (!empty($value->request) && $value->request != 'null') ? $value->request : '-',
        (!empty($value->response) && $value->response != 'null') ? $value->response : '-',
      ];
    }

    ob_start();
    $outputCsv = fopen('php://output', 'w');
    fputcsv($outputCsv, $header, ",");
    foreach ($rows as $row) {
      fputcsv($outputCsv, $row, ",");
    }
    header('Cache-Control: max-age=0');
    header("Expires: 0");
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header('Content-type: application/csv; charset=utf-8');
    header('Content-Disposition: attachment;filename="sibs_api_audits.csv"');
    header("Content-Transfer-Encoding: binary");
    fpassthru($outputCsv);
    fclose($outputCsv);
    exit;
  }

}
