<?php

namespace Drupal\sibs_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure sibs_api settings for this site.
 */
class SibsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sibs_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sibs_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sibs_api.settings');
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client-Id'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal ID'),
      '#default_value' => $config->get('terminal_id'),
      '#required' => TRUE,
    ];

    $form['token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Token'),
      '#default_value' => $config->get('token'),
      '#required' => TRUE,
    ];

    $form['entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment Service Entity'),
      '#default_value' => $config->get('entity'),
      '#required' => TRUE,
    ];

    $form['webhook_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook Secret'),
      '#default_value' => $config->get('webhook_secret'),
      '#required' => TRUE,
    ];

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Integration Endpoint'),
      '#default_value' => $config->get('endpoint'),
      '#required' => TRUE,
    ];

    $form['spg'] = [
      '#type' => 'details',
      '#title' => $this->t('SPG'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['spg']['spg_return_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Return URL'),
      '#default_value' => $config->get('spg_return_url'),
      '#required' => TRUE,
    ];
    $form['spg']['spg_validity'] = [
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#title' => $this->t('Validity'),
      '#default_value' => $config->get('spg_validity'),
      '#description' => $this->t('Number of days the payment is valid'),
      '#required' => TRUE,
    ];
    $form['spg']['formstyle'] = [
      '#type' => 'details',
      '#title' => $this->t('SPG formStyle'),
      '#open' => FALSE,
    ];
    $form['spg']['formstyle']['spg_formStyle_layout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('layout'),
      '#default_value' => (!empty($config->get('spg_formStyle.layout'))) ? $config->get('spg_formStyle.layout') : 'default',
      '#required' => TRUE,
    ];
    $form['spg']['formstyle']['spg_formStyle_theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('theme'),
      '#default_value' => (!empty($config->get('spg_formStyle.theme'))) ? $config->get('spg_formStyle.theme') : 'default',
      '#required' => TRUE,
    ];
    $form['spg']['formstyle']['color'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('color'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['spg']['formstyle']['color']['spg_formStyle_color_primary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('primary'),
      '#default_value' => $config->get('spg_formStyle.color.primary'),
    ];
    $form['spg']['formstyle']['color']['spg_formStyle_color_secondary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('secondary'),
      '#default_value' => $config->get('spg_formStyle.color.secondary'),
    ];
    $form['spg']['formstyle']['color']['spg_formStyle_color_border'] = [
      '#type' => 'textfield',
      '#title' => $this->t('border'),
      '#default_value' => $config->get('spg_formStyle.color.border'),
    ];
    $form['spg']['formstyle']['color']['spg_formStyle_color_surface'] = [
      '#type' => 'textfield',
      '#title' => $this->t('surface'),
      '#default_value' => $config->get('spg_formStyle.color.surface'),
    ];
    $form['spg']['formstyle']['color']['header'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('header'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['spg']['formstyle']['color']['header']['spg_formStyle_color_header_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('text'),
      '#default_value' => (!empty($config->get('spg_formStyle.color.header.text'))) ? $config->get('spg_formStyle.color.header.text') : '#5C5C5C',
      '#required' => TRUE,
    ];
    $form['spg']['formstyle']['color']['header']['spg_formStyle_color_header_background'] = [
      '#type' => 'textfield',
      '#title' => $this->t('background'),
      '#default_value' => (!empty($config->get('spg_formStyle.color.header.background'))) ? $config->get('spg_formStyle.color.header.background') : '#FFF',
      '#required' => TRUE,
    ];
    $form['spg']['formstyle']['color']['body'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('body'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['spg']['formstyle']['color']['body']['spg_formStyle_color_body_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('text'),
      '#default_value' => (!empty($config->get('spg_formStyle.color.body.text'))) ? $config->get('spg_formStyle.color.body.text') : '#5C5C5C',
      '#required' => TRUE,
    ];
    $form['spg']['formstyle']['color']['body']['spg_formStyle_color_body_background'] = [
      '#type' => 'textfield',
      '#title' => $this->t('background'),
      '#default_value' => (!empty($config->get('spg_formStyle.color.body.background'))) ? $config->get('spg_formStyle.color.body.background') : '#FFF',
      '#required' => TRUE,
    ];
    $form['spg']['formstyle']['spg_formStyle_font'] = [
      '#type' => 'textfield',
      '#title' => $this->t('font'),
      '#default_value' => $config->get('spg_formStyle.font'),
    ];

    $form['mb'] = [
      '#type' => 'details',
      '#title' => $this->t('MB'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['mb']['mb_validity'] = [
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#title' => $this->t('Validity'),
      '#default_value' => $config->get('mb_validity'),
      '#description' => $this->t('Number of days the payment is valid'),
      '#required' => TRUE,
    ];

    $form['mbway'] = [
      '#type' => 'details',
      '#title' => $this->t('MB Way'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['mbway']['mbway_validity'] = [
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#title' => $this->t('Validity'),
      '#default_value' => $config->get('mbway_validity'),
      '#description' => $this->t('Number of days the payment is valid'),
      '#required' => TRUE,
    ];

    $form['audits'] = [
      '#type' => 'details',
      '#title' => $this->t('Audits'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['audits']['audits_expire'] = [
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#title' => $this->t('Delete records older than'),
      '#default_value' => $config->get('audits_expire'),
      '#description' => $this->t('In days'),
    ];

    $form['errors'] = [
      '#type' => 'select',
      '#title' => $this->t('Error handling'),
      '#default_value' => $config->get('errors'),
      '#options' => [
        'none' => $this->t("None"),
        'log' => $this->t("Database logging"),
        'screen' => $this->t("Output in page"),
        'both' => $this->t("Both"),
      ],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sibs_api.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('terminal_id', $form_state->getValue('terminal_id'))
      ->set('entity', $form_state->getValue('entity'))
      ->set('token', $form_state->getValue('token'))
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->set('webhook_secret', $form_state->getValue('webhook_secret'))
      ->set('spg_return_url', $form_state->getValue('spg_return_url'))
      ->set('spg_validity', $form_state->getValue('spg_validity'))
      ->set('spg_formStyle.layout', $form_state->getValue('spg_formStyle_layout'))
      ->set('spg_formStyle.theme', $form_state->getValue('spg_formStyle_theme'))
      ->set('spg_formStyle.color.primary', $form_state->getValue('spg_formStyle_color_primary'))
      ->set('spg_formStyle.color.secondary', $form_state->getValue('spg_formStyle_color_secondary'))
      ->set('spg_formStyle.color.border', $form_state->getValue('spg_formStyle_color_border'))
      ->set('spg_formStyle.color.surface', $form_state->getValue('spg_formStyle_color_surface'))
      ->set('spg_formStyle.color.header.text', $form_state->getValue('spg_formStyle_color_header_text'))
      ->set('spg_formStyle.color.header.background', $form_state->getValue('spg_formStyle_color_header_background'))
      ->set('spg_formStyle.color.body.text', $form_state->getValue('spg_formStyle_color_body_text'))
      ->set('spg_formStyle.color.body.background', $form_state->getValue('spg_formStyle_color_body_background'))
      ->set('spg_formStyle.font', $form_state->getValue('spg_formStyle_font'))
      ->set('mb_validity', $form_state->getValue('mb_validity'))
      ->set('mbway_validity', $form_state->getValue('mbway_validity'))
      ->set('audits_expire', $form_state->getValue('audits_expire'))
      ->set('errors', $form_state->getValue('errors'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
