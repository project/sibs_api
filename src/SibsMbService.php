<?php

namespace Drupal\sibs_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Utility\Random;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Class for Sibs Mb Service.
 *
 * @package Drupal\sibs_api
 */
class SibsMbService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $http_client) {
    $this->configFactory = $configFactory->get('sibs_api.settings');
    $this->httpClient = $http_client;
  }

  /**
   * First request to prepare the checkout.
   *
   * @param float $amount
   *   The payment amount.
   * @param string $description
   *   The payment description.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function prepareCheckout(float $amount, string $description) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments';
    $date = gmdate('Y-m-d\TH:i:s.v\Z');
    $date_end = gmdate("Y-m-d\TH:i:s.v\Z", strtotime('+' . $this->configFactory->get('mb_validity') . ' days'));
    $random = new Random();
    $json_data = [
      "merchant" => [
        "terminalId" => (int) $this->configFactory->get('terminal_id'),
        "channel" => "web",
        "merchantTransactionId" => $random->name(24, TRUE),
      ],
      "transaction" => [
        "transactionTimestamp" => $date,
        "description" => $description,
        "moto" => FALSE,
        "paymentType" => "PURS",
        "amount" => [
          "value" => $amount,
          "currency" => "EUR",
        ],
        "paymentMethod" => [
          'REFERENCE',
        ],
        "paymentReference" => [
          "initialDatetime" => $date,
          "finalDatetime" => $date_end,
          "maxAmount" => [
            "value" => $amount,
            "currency" => "EUR",
          ],
          "minAmount" => [
            "value" => $amount,
            "currency" => "EUR",
          ],
          "entity" => $this->configFactory->get('entity'),
        ],
      ],
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'body' => json_encode($json_data, JSON_PRETTY_PRINT),
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'prepareCheckout', NULL, $json_data);
  }

  /**
   * Generate mb reference.
   *
   * @param string $id
   *   Transaction_id.
   * @param array $context
   *   Payment context.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function generateReference(string $id, array $context) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments/' . $id . '/service-reference/generate';

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Digest ' . $context['transactionSignature'],
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'generateReference', $id, NULL);
  }

  /**
   * Check the status of your transaction making a GET request.
   *
   * @param string $id
   *   Payment_id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function getpaymentStatus(string $id) {
    $uri = $this->configFactory->get('endpoint') . '/api/v1/payments/' . $id . '/status';

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json; charset=utf-8',
          'Authorization' => 'Bearer ' . $this->configFactory->get('token'),
          'X-IBM-Client-Id' => $this->configFactory->get('client_id'),
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $request = FALSE;
    }

    return _sibs_api_handle_response($request, 'getpaymentStatus', $id, NULL);
  }

}
