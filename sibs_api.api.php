<?php

/**
 * @file
 * Hooks for sibs_api module.
 */

/**
 * Fired when SIBS reaches the website webhook.
 *
 * @param string $result
 *   The text element to be translated.
 */
function hook_sibs_api_webhook_alter(&$result) {

}
